import { questionArr } from "../data/data.js";
import {
  renderFullQuestion,
  renderQuestionAnswers,
  renderSoThuTu,
  checkValue,
} from "../controllers/controller.js";
var index = 0;
renderFullQuestion(
  renderQuestionAnswers(questionArr[index]),
  renderSoThuTu(index, questionArr.length)
);

let nextQuestion = () => {
  checkValue(questionArr[index]),
    index++,
    renderQuestionAnswers(questionArr[index]),
    renderSoThuTu(index, questionArr.length);
};
window.nextQuestion = nextQuestion;
