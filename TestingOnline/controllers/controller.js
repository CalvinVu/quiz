export let renderQuestionAnswers = (question) => {
  let contentHTMLQuestion = `<h2>${question.content}</h2>`;
  let contentHTMLAnswers = "";
  if (question.questionType == 1) {
    question.answers.forEach((item) => {
      contentHTMLAnswers += `<div class="form-check">
          <input type="radio" class="form-check-input" name="checkname" id="${item.id}" value="checkedValue">
        ${item.content}
      </div>`;
    });
  } else {
    contentHTMLAnswers += `<div class="form-group">
    <input type="text"
      class="form-control" name="" id="" aria-describedby="helpId" placeholder="">
  </div>`;
  }
  document.getElementById("contentQuiz").innerHTML =
    contentHTMLQuestion + contentHTMLAnswers;
  return contentHTMLAnswers;
};

export let renderSoThuTu = (index, arrlength) => {
  document.getElementById("currentStep").innerHTML = `${
    index + 1
  }/${arrlength}`;
};
export let renderFullQuestion = () => {};

export let checkValue = (question) => {
  let id = document.querySelector('input[name="checkname"]:checked').value;
  let viTri = question.answers.findIndex((item) => {
    item.id = id;
  });
  console.log(viTri);
};
window.checkValue = checkValue;
